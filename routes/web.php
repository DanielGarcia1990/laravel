<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 * Ruta de ejemplo
 */
Route::get('/test', function(){
    return 'Laravel 5.5';
});

Route::get('/usuarios','UserController@index');

Route::get('/saludo/{name}/{nickname?}', function($name, $nickname = null){
    if ($nickname){
        return "Bienvenido {$name}, tu apodo es {$nickname}";
    } else{
        return "Bienvenido {$name}, no tienes apodo";
    }
});

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/', function () {
    return view('form');
});

