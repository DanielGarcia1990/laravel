@extends('users')

@section('title', 'Usuarios', 'Puesto')

@section('content')
    <div class="container-fluid">
        <h1>{{ $title }}</h1>
    </div>

    <ul>
        @forelse ($users as $user)
        <div class="row">
            <div class="col-sm-3">
                <div class="card" style="width:220px">
                    <img class="card-img-top" src="avatar.png" alt="Foto Usuario" style="width:100%">
                    <div class="card-body">
                        <h4 class="card-title">Usuario {{ $user->id}} </h4>
                        <a href="#myModal" class="btn btn-primary" data-toggle="modal" title="Información de: {{$user->nombre}}" data-target="#myModal-{{$user->id}}">Ver Perfil</a>
                    </div>
                </div>
            </div>
        </div>

        <!-- The Modal -->
        <div class="modal fade" id="myModal-{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Datos del Usuario</h4>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        Nombre: {{ $user->nombre }}
                        <br>
                        Email:  {{ $user->email }}
                        <br>
                        Puesto de Trabajo: {{ $user->puesto_id}}

                        @if ($user->puesto_id == 0)
                            <p>No tiene un puesto de trabajo asignado.</p>
                        @endif
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
        <br>
        @empty
            <li>No hay usuarios registrados.</li>
        @endforelse
    </ul>
    
@endsection
