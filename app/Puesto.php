<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Puesto extends Model
{
    protected $fillable = ['nombre'];
    public function users(){
        return $this->hasMany(User::class);
    }
}
