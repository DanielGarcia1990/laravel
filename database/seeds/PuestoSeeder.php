<?php

use App\Puesto;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class PuestoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
/*
        Puesto::create([
            'nombre'=> 'CEO',
        ]);

        Puesto::create([
            'nombre'=> 'Front-End Developer',
        ]);

        Puesto::create([
            'nombre'=> 'Back-End Developer',
        ]);

        Puesto::create([
            'nombre'=> 'Administrativo',
        ]);

        Puesto::create([
            'nombre'=> 'Diseñador Web',
        ]);
*/
       //factory(Puesto::class, 4)->create();

        //Insercción de nuevos datos en la base de datos
        DB::table('puesto')->insert([
            'nombre'=> 'CEO',
        ]);

        DB::table('puesto')->insert([
            'nombre'=> 'Front-End Developer',
        ]);

        DB::table('puesto')->insert([
            'nombre'=> 'Back-End Developer',
        ]);

        DB::table('puesto')->insert([
            'nombre'=> 'Administrativo',
        ]);

        DB::table('puesto')->insert([
            'nombre'=> 'Diseñador Web',
        ]);
        
    }
}
