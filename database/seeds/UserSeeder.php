<?php

use App\User;
use App\Puesto;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //Esto genera datos falsos en la BD para hacer pruebas.
        //17 -> Numero de usuarios que va a crear en la BD.
        factory(User::class, 17)->create();

        User::create([
            'nombre'=> 'Daniel Garcia',
            'email' => 'danigd71@gmail.com',
            'contraseña' => bcrypt('laravel'),
        ]);

        User::create([
            'nombre'=> 'Julio Garcia',
            'email' => 'julito@gmail.com',
            'contraseña' => bcrypt('laravel2'),
        ]);
    }
}
